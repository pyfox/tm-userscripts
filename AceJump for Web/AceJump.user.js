// ==UserScript==
// @name         AceJump for Web
// @namespace    http://tampermonkey.net/
// @version      0.26
// @description  Navigate any web page using keyboard shortcuts. Press Ctrl+, to start. Inspired by AceJump.
// @author       Pyfips
// @match        *://*/*
// @grant        none
// ==/UserScript==
(function () {
    'use strict';
    const firstLetters = 'fdsawertgxcvb'; // First letter of the letterpairs
    const secondLetters = 'jkuiopnmzh'; // second letter of the letterpairs
    const maxParentDepth = 8; // Look up up to that much parents before assuming that it is no link (for div & span)
    let viewDivs = [];

    let shortcutToCoord = {};
    let isActive = false;
    let currentKeyStroke = '';
    const letterPairs = [];
    for (let i of firstLetters) {
        for (let j of secondLetters) {
            letterPairs.push(i+j);
            letterPairs.push(j+i);
        }
    }
    for (let pair of [...letterPairs]) {
        letterPairs.push('q'+pair);
    }

    // Checks if two elements are related to each other.
    function isRelatedElement(elementA, elementB) {
        try {
            let currentNode = elementA;
            for (let i = 0; i < maxParentDepth; i++) {
                currentNode = currentNode.parentNode;
                if (currentNode === elementB) {
                    return true;
                }
            }
            return false;
        } catch(error) {
            return false;
        }
    }
    // Checks if a element is hidden behind another element.
    function isBehindOtherElement(element, currentDoc) {
        const boundingRect = element.getBoundingClientRect();
        const left = boundingRect.left + 1;
        const right = boundingRect.right - 1;
        const top = boundingRect.top + 1;
        const bottom = boundingRect.bottom - 1;
        const elemTopLeft = currentDoc.elementFromPoint(left, top);
        const elemTopRight = currentDoc.elementFromPoint(right, top);
        const elemBottomLeft = currentDoc.elementFromPoint(left, bottom);
        const elemBottomRight = currentDoc.elementFromPoint(right, bottom);
        const elementCenter = currentDoc.elementFromPoint(left + (right - left)/2, top + (bottom - top)/2);
        if(elementCenter !== element && elemTopLeft !== element && elemTopRight !== element && elemBottomLeft !== element && elemBottomRight !== element) {
            return !((isRelatedElement(elementCenter, element)
                    || isRelatedElement(elemBottomLeft, element)
                    || isRelatedElement(elemTopRight, element)
                    || isRelatedElement(elemTopLeft, element)
                    || isRelatedElement(elemBottomRight, element)));
        }
        return false;
    }

    function isAttachedToClickableParent(element) {
        let parentIsClickable = false;
        let currentParent = element.parentNode;
        for (let i = 0; i < maxParentDepth; i++) {
            if (currentParent == null) {
                return false;
            }
            if (['BUTTON', 'A'].indexOf(currentParent.tagName) !== -1) {
                return true;
            }
            currentParent = currentParent.parentNode;
        }
        return false;
    }
    // Create overlay for elements that can be navigated.
    function doAceJump() {
        let x = performance.now();
        isActive = true;
        let counter = 0;
        shortcutToCoord = {};
        viewDivs = [];
        function tagElements(currentDoc) {
            for (let elem of currentDoc.getElementsByTagName('iframe')) {
                try {
                    tagElements(elem.contentWindow.document);
                } catch (e) {
                    // e.g. Cross orign
                    continue;
                }
            }
            let elems = [];
            for (let tagname of ['button', 'select', 'a', 'input', 'textarea', 'img', 'svg', 'div', 'li', 'span']) {
                elems.push(...currentDoc.getElementsByTagName(tagname));
            }
            let baseElem = currentDoc.createElement('div');
            baseElem.style.position = 'fixed';
            baseElem.style.fontSize = '1rem';
            baseElem.style.fontWeight = 'bold';
            baseElem.style.width = '2.3rem';
            baseElem.style.height = '1.5rem';
            baseElem.style.backgroundColor = 'rgba(50, 50, 50, 0.8)';
            baseElem.style.color = '#fff';
            baseElem.style.textAlign = 'center';
            baseElem.style.zIndex = '99999';
            baseElem.style.pointerEvents = 'none';
            for (let elem of elems) {
                const rect = elem.getBoundingClientRect();
                if (rect.top + rect.bottom + rect.width + rect.height === 0 || rect.top < 0 || rect.bottom > window.innerHeight) {
                    // ignore elements that are not in current view
                    continue;
                }
                let elemStyle = getComputedStyle(elem, null);
                if (['IMG', 'svg'].includes(elem.tagName) && elemStyle.cursor !== 'pointer') {
                    continue;
                }
                if (['DIV', 'LI', 'SPAN'].includes(elem.tagName)) {
                    if (elemStyle.cursor !== 'pointer' && elem.getAttribute('role') != 'button') {
                        continue;
                    } else if (elem.parentNode && getComputedStyle(elem.parentNode).cursor === 'pointer') {
                        // If parent itself is clickable, ignore child. Otherwise, nested spans could spam on some pages.
                        continue;
                    }
                }
                if (elemStyle.opacity === '0' || elemStyle.display === 'None' || elemStyle.visibility === 'hidden') {
                    // ignore invisible elements
                    continue;
                }
                if (isAttachedToClickableParent(elem)) {
                    continue;
                }
                if (isBehindOtherElement(elem, currentDoc)) {
                    // ignore elements that are hidden behind other elements
                    continue;
                }
                const y = rect.y + rect.height / 2 - 10;
                const x = rect.x + rect.width / 2 - 20;
                shortcutToCoord[letterPairs[counter]] = [elem, currentDoc];
                let newElem = baseElem.cloneNode();
                newElem.style.top = '' + y + 'px';
                newElem.style.left = '' + x + 'px';
                newElem.innerHTML = letterPairs[counter];
                viewDivs.push([newElem, currentDoc]);
                counter += 1;
            }
        }
        tagElements(window.document);
        console.log("Analysis took", performance.now() - x);
        let y = performance.now();
        for (let [elem, doc] of viewDivs) {
            doc.body.appendChild(elem);
        }
        console.log("Drawing took", performance.now() - y);
    }
    // Simulates the click on an element and resets the state.
    function clickSim(element, doc, hoverOnly) {
        // First clean up...
        currentKeyStroke = '';
        isActive = false;
        window.document.removeEventListener('scroll', resetAcejump);
        window.document.removeEventListener('click', resetAcejump);
        for (let [elem, d] of viewDivs) {
            d.body.removeChild(elem);
        }
        viewDivs = [];
        // Then click
        if (element) {
            let finalElement = element;
            const rect = element.getBoundingClientRect();
            let elementToClick = doc.elementFromPoint(rect.left + (rect.right - rect.left)/2, rect.top + (rect.bottom - rect.top)/2);
            while (true) {
                try {
                    elementToClick.focus(); // For text inputs
                    let mouseEvent = new MouseEvent('mouseover', {view: window, bubbles: true, cancelable: true});
                    elementToClick.dispatchEvent(mouseEvent);
                    if (hoverOnly) {
                        return;
                    }
                    mouseEvent = document.createEvent("MouseEvents");
                    mouseEvent = new MouseEvent('mousedown', {view: window, bubbles: true, cancelable: true});
                    elementToClick.dispatchEvent(mouseEvent);
                    mouseEvent = new MouseEvent('mouseup', {view: window, bubbles: true, cancelable: true});
                    elementToClick.dispatchEvent(mouseEvent);
                    elementToClick.click();
                    console.log('Clicked on', elementToClick);
                    break;
                } catch (e) {
                    elementToClick = elementToClick.parentNode;
                }
            }
        }
    }
    function resetAcejump() {
        clickSim(null, null);
    }
    // Key event listener
    function onKey(event) {
      console.log(event)
        if (!isActive && (event.ctrlKey && event.key === ',' || event.key === ';' && !event.shiftKey)) {
            event.preventDefault();
            event.stopPropagation();
            window.document.addEventListener('scroll', resetAcejump);
            window.document.addEventListener('click', resetAcejump);
            doAceJump();
            return;
        } else if (!isActive) {
            return;
        }
        if (['ArrowUp', 'ArrowDown', 'PageUp', 'PageDown'].includes(event.key)) {
            clickSim(null, null);
            return;
        }
        if (event.key === 'Backspace') {
            event.preventDefault();
            event.stopPropagation();
            currentKeyStroke = '';
            return;
        }
        if (['Shift', 'CapsLock'].includes(event.key)) {return;}
        event.preventDefault();
        event.stopPropagation();
        if (event.key === 'Escape') {
            clickSim(null, null);
            return;
        }
        currentKeyStroke += event.key.toLowerCase();
        if (shortcutToCoord[currentKeyStroke]) {
            clickSim(...shortcutToCoord[currentKeyStroke], event.shiftKey);
        }
    }
    document.addEventListener("keydown", onKey, true);
})();
