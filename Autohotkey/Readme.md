Obviously this is no Tampermonkey script, but Authotkey.

It basically makes the Capslock key activate a "command mode". 

This command mode includes a Vim inspired navigation with the following shortcuts:
* hjkl to move the cursor left, down, up, right
* u and p to move the cursor to line start / line end
* i and o will move the cursor to the previous / next word by pressing Ctrl+Left/Right
* Use shift and one of the above commands to select
* E and D will move page up / down

The other keys are free to use for hot keys of any application, e.g. an IDE. Each key is mapped to <Ctrl+Alt(+Shift)+Key>
There are some special keys that will disable the Command mode:
* ö/Ö: I use it for adding text in the previous / next line.
* ; will press <Ctrl+Alt+;> and disable command mode for 1.5 seconds. This is meant to be used with AceJump.

Also, command mode will be disabled after certain hotkeys:
* Ctrl+t
* Ctrl+f
* Ctrl+r
* Ctrl+h
* Ctrl+A (IntelliJ's "Find Action")

Note that even in command mode, pressing Ctrl+<any key> or Alt+<any key> will be pressed without modification


##Usage
To use, install Autohotkey and start the script. You may want to put it in shell:startup
