SetStoreCapslockMode, Off

SplashImageGUI(Picture)
{
Gui, XPT99:Margin , 0, 0
Gui, XPT99:Add, Picture,, %Picture%
Gui, XPT99:Color, ECE9D8
Gui, XPT99:+LastFound -Caption +AlwaysOnTop +ToolWindow -Border
Winset, TransColor, 000000
Gui, XPT99:Show, Y15 NoActivate
}


CapsLock::
If GetKeyState("CapsLock", "T")
	Gui, XPT99:Destroy
else
	SplashImageGUI("test.png")
SendInput {CapsLock}

#If GetKeyState("CapsLock", "T") && !GetKeyState("LCTRL", "P") && !GetKeyState("RCTRL", "P") && !GetKeyState("Alt", "P")
a::SendInput {Ctrl down}{Alt down}a{Alt up}{Ctrl up}
+a::SendInput {Ctrl down}{Alt down}+a{Alt up}{Ctrl up}
b::SendInput {Ctrl down}{Alt down}b{Alt up}{Ctrl up}
+b::SendInput {Ctrl down}{Alt down}+b{Alt up}{Ctrl up}
c::SendInput {Ctrl down}{Alt down}c{Alt up}{Ctrl up}
+c::SendInput {Ctrl down}{Alt down}+c{Alt up}{Ctrl up}
d::SendInput {PgDn}
+d::SendInput {Ctrl down}{Alt down}+d{Alt up}{Ctrl up}
e::SendInput {PgUp}
+e::SendInput {Ctrl down}{Alt down}+e{Alt up}{Ctrl up}
f::SendInput {Ctrl down}{Alt down}f{Alt up}{Ctrl up}
+f::SendInput {Ctrl down}{Alt down}+f{Alt up}{Ctrl up}
g::SendInput {Ctrl down}{Alt down}g{Alt up}{Ctrl up}
+g::SendInput {Ctrl down}{Alt down}+g{Alt up}{Ctrl up}
h::SendInput {Left}
+h::SendInput +{Left}
i::SendInput ^{Left}
+i::SendInput ^+{Left}
j::SendInput {Down}
+j::SendInput +{Down}
k::SendInput {Up}
+k::SendInput +{Up}
l::SendInput {Right}
+l::SendInput +{Right}
m::SendInput {Ctrl down}{Alt down}m{Alt up}{Ctrl up}
+m::SendInput {Ctrl down}{Alt down}+m{Alt up}{Ctrl up}
n::SendInput {Ctrl down}{Alt down}n{Alt up}{Ctrl up}
+n::SendInput {Ctrl down}{Alt down}+n{Alt up}{Ctrl up}

o::SendInput ^{Right}
+o::SendInput ^+{Right}
p::SendInput {End}
+p::SendInput +{End}
q::SendInput {Ctrl down}{Alt down}q{Alt up}{Ctrl up}
+q::SendInput {Ctrl down}{Alt down}+q{Alt up}{Ctrl up}
r::SendInput {Ctrl down}{Alt down}r{Alt up}{Ctrl up}
+r::SendInput {Ctrl down}{Alt down}+r{Alt up}{Ctrl up}
s::SendInput {Ctrl down}{Alt down}s{Alt up}{Ctrl up}
+s::SendInput {Ctrl down}{Alt down}+s{Alt up}{Ctrl up}
t::SendInput {Ctrl down}{Alt down}t{Alt up}{Ctrl up}
+t::SendInput {Ctrl down}{Alt down}+t{Alt up}{Ctrl up}
u::SendInput {Home}
+u::SendInput +{Home}
v::SendInput {Ctrl down}{Alt down}v{Alt up}{Ctrl up}
+v::SendInput {Ctrl down}{Alt down}+v{Alt up}{Ctrl up}
w::SendInput {Ctrl down}{Alt down}+w{Alt up}{Ctrl up}
+w::SendInput {Ctrl down}{Alt down}+w{Alt up}{Ctrl up}
x::SendInput {Ctrl down}{Alt down}x{Alt up}{Ctrl up}
+x::SendInput {Ctrl down}{Alt down}+x{Alt up}{Ctrl up}
y::SendInput {Ctrl down}{Alt down}y{Alt up}{Ctrl up}
+y::SendInput {Ctrl down}{Alt down}+y{Alt up}{Ctrl up}
z::SendInput {Ctrl down}{Alt down}z{Alt up}{Ctrl up}
+z::SendInput {Ctrl down}{Alt down}+z{Alt up}{Ctrl up}
1::SendInput {Ctrl down}{Alt down}1{Alt up}{Ctrl up}
+1::SendInput {Ctrl down}{Alt down}+1{Alt up}{Ctrl up}
2::SendInput {Ctrl down}{Alt down}2{Alt up}{Ctrl up}
+2::SendInput {Ctrl down}{Alt down}+2{Alt up}{Ctrl up}
3::SendInput {Ctrl down}{Alt down}3{Alt up}{Ctrl up}
+3::SendInput {Ctrl down}{Alt down}+3{Alt up}{Ctrl up}
4::SendInput {Ctrl down}{Alt down}4{Alt up}{Ctrl up}
+4::SendInput {Ctrl down}{Alt down}+4{Alt up}{Ctrl up}
5::SendInput {Ctrl down}{Alt down}5{Alt up}{Ctrl up}
+5::SendInput {Ctrl down}{Alt down}+5{Alt up}{Ctrl up}
6::SendInput {Ctrl down}{Alt down}6{Alt up}{Ctrl up}
+6::SendInput {Ctrl down}{Alt down}+6{Alt up}{Ctrl up}
7::SendInput {Ctrl down}{Alt down}7{Alt up}{Ctrl up}
+7::SendInput {Ctrl down}{Alt down}+7{Alt up}{Ctrl up}
8::SendInput {Ctrl down}{Alt down}8{Alt up}{Ctrl up}
+8::SendInput {Ctrl down}{Alt down}+8{Alt up}{Ctrl up}
9::SendInput {Ctrl down}{Alt down}9{Alt up}{Ctrl up}
+9::SendInput {Ctrl down}{Alt down}+9{Alt up}{Ctrl up}
0::SendInput {Ctrl down}{Alt down}0{Alt up}{Ctrl up}
+0::SendInput {Ctrl down}{Alt down}+0{Alt up}{Ctrl up}
.::SendInput {Ctrl down}{Alt down}.{Alt up}{Ctrl up}
+.::SendInput {Ctrl down}{Alt down}+.{Alt up}{Ctrl up}
,::
SendInput {Ctrl down}{Alt down},{Alt up}{Ctrl up}
SetCapsLockState, off
Gui, XPT99:Destroy
Input, L, L1
Send %L%
sleep, 1000
SetCapsLockState, on
SplashImageGUI("test.png")
return
+,::SendInput {Ctrl down}{Alt down}+,{Alt up}{Ctrl up}
SC02B::SendInput {Ctrl down}{Alt down}{#}{Alt up}{Ctrl up}
+SC02B::SendInput {Ctrl down}{Alt down}+{#}{Alt up}{Ctrl up}
SC01B::SendInput {Ctrl down}{Alt down}{+}{Alt up}{Ctrl up}
+SC01B::SendInput {Ctrl down}{Alt down}+{+}{Alt up}{Ctrl up}
SC029::SendInput {Ctrl down}{Alt down}{^}{Alt up}{Ctrl up}
+SC029::SendInput {Ctrl down}{Alt down}+{^}{Alt up}{Ctrl up}
SC028::SendInput {Ctrl down}{Alt down}{SC028}{Alt up}{Ctrl up} ; ä
+SC028::SendInput {Ctrl down}{Alt down}+{SC028}{Alt up}{Ctrl up} ; ä
SC027:: ; ö
SetCapsLockState, off
Gui, XPT99:Destroy
SendInput {Ctrl down}{Alt down}{SC027}{Alt up}{Ctrl up}
return
+SC027:: ; ö
SetCapsLockState, off
Gui, XPT99:Destroy
SendInput {Ctrl down}{Alt down}+{SC027}{Alt up}{Ctrl up}
return
SC01A::SendInput {Ctrl down}{Alt down}{SC01A}{Alt up}{Ctrl up} ; ü
+SC01A::SendInput {Ctrl down}{Alt down}+{SC01A}{Alt up}{Ctrl up} ; ü
Backspace::SendInput {Ctrl down}{Alt down}{Backspace}{Alt up}{Ctrl up}
+Backspace::SendInput {Ctrl down}{Alt down}+{Backspace}{Alt up}{Ctrl up}

#If GetKeyState("CapsLock", "T")
^+a::
SendInput {Ctrl down}+a{Ctrl up}
SetCapsLockState, off
Gui, XPT99:Destroy
return
^t::
SendInput ^t
SetCapsLockState, off
Gui, XPT99:Destroy
return
^f::
SendInput ^f
SetCapsLockState, off
Gui, XPT99:Destroy
return
^r::
SendInput ^r
SetCapsLockState, off
Gui, XPT99:Destroy
return
^h::
SendInput ^h
SetCapsLockState, off
Gui, XPT99:Destroy
return
#If
