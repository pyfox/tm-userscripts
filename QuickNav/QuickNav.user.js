// ==UserScript==
// @name         QuickNav
// @namespace    http://tampermonkey.net/
// @version      0.14
// @description  Easy scrolling without arrows (Use HJKL when capslock is active)
// @author       Pyfips
// @match        *://*/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    function _onKey(event) {
        if (['J', 'K', 'H', 'L'].includes(event.key) && !event.shiftKey) {
            event.preventDefault();
            event.stopPropagation();
        } else {
            return;
       } 

        let maxSize = 0;
        let elementToScroll;
        let elems = document.getElementsByTagName('div');
        if (document.body.scrollHeight > window.innerHeight) {
            elementToScroll = window;
        } else {
            for (let elem of elems) {
                if ((elem.scrollHeight - elem.clientHeight) * elem.clientWidth > maxSize) {
                    maxSize = (elem.scrollHeight - elem.clientHeight) * elem.clientWidth;
                    elementToScroll = elem;
                }
            }
        }
        if (true) {
            if (event.key === 'J' && !event.shiftKey) {
                elementToScroll.scrollBy(0, 200);
            }
            if (event.key === 'K' && !event.shiftKey) {
                elementToScroll.scrollBy(0, -200);
            }
            if (event.key === 'H' && !event.shiftKey) {
                elementToScroll.scrollBy(-200, 0);
            }
            if (event.key === 'L' && !event.shiftKey) {
                elementToScroll.scrollBy(200, 0);
            }
        }
    }
    document.addEventListener("keydown", _onKey, false);
})();
